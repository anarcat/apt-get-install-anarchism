apt-get install anarchism
=========================

Those are designs of the original "apt-get install anarchism" (AGIA)
t-shirt adapted to various purposes.

 * `apt-get-install-anarchism.svg` - the original design
 * `apt-get-install-anarchism-black-on-white.svg` - simpler design,
   only black on white, without text. useful to make keyboard keys or
   smaller screenprinter logos

Keyboard layout
---------------

In `wasd-keyboard.svg`, you can see a modified [WASD keyboard][]
design that replaces the "Windows" (AKA "Tux" or "Meta") keys with the
AGIA design. It basically takes the above black and white logo and
puts it in the right location. We also add the [IWW black cat][] on
the escape key, in case our politics were not clear enough. The
keyboard layout is also peculiar: it uses the "Large Font" layour with
"centered modifiers" with "media keys" enabled.

[WASD keyboard]: http://www.wasdkeyboards.com/
[IWW black cat]: https://en.wikipedia.org/wiki/Black_cat#Anarcho-syndicalism

It looks like this when rendered by WASD:

![WASD rendering of the layout](wasd-keyboard-rendered.png)

And here's a live action picture (no riot porn here sorry):

![Actual picture of the keyboard](wasd-keyboard-photo.jpg)
